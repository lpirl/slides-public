#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date

extensions = [
    'sphinx.ext.graphviz',
    'sphinx.ext.imgmath',
    'matplotlib.sphinxext.plot_directive',
    'hieroglyph',
]

# required, so that serving the slides under "/" works in the browser
master_doc = 'index'

exclude_patterns = [
    '.DS_Store',
    'Thumbs.db',
    'README*',
    '**/README*',
    'build',
    'inc',
]

# required, so that the HTML title does not get suffixed with
# "… documentation"
html_title = ""

html_scaled_image_link = False

slide_theme_path = ['themes']
slide_theme = 'simple-slides-dark'
slide_footer = '　'.join((
    'software fault injection tools',
    'Software Reliability Engineering SoSe18',
    'lukas.pirl@hpi.de',
    '23.05.2018',
))
slide_numbers = True

pygments_style = 'monokai'

graphviz_output_format = 'svg'
graphviz_dot_args = [
    '-Gfontname=Ubuntu,sans',
    '-Efontname=Ubuntu,sans',
    '-Nfontname=Ubuntu,sans',
]

from matplotlib.sphinxext.plot_directive import TEMPLATE as PLOT_TEMPLATE
plot_formats = ['svg', 'png']
plot_template = PLOT_TEMPLATE + """
.. only:: slides

  {% for img in images %}
  .. figure:: {{ build_dir }}/{{ img.basename }}.{{ default_fmt }}
    {% for option in options -%}
    {{ option }}
    {% endfor %}
    {{ caption }}
  {% endfor %}
"""

imgmath_image_format = 'svg'

rst_prolog = '.. include:: prolog.rst'
rst_epilog = '.. include:: ../0-generic/epilog.rst'
