import matplotlib.pyplot as plt

hits = {
  2011: 7,
  2012: 320,
  2013: 317,
  2014: 542,
  2015: 208,
  2016: 182,
  2017: 112,
}

plt.bar(hits.keys(), hits.values())
plt.title('hits when searching for "trinity site:lkml.org/ inurl:/20xx/')
plt.show()
