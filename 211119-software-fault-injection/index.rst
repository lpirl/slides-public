.. todo:

  * Dependability
  * WiFi example



.. rst-class:: no-footer no-slide-no

========================
software fault injection
========================

.. raw:: html

  <style>
  h1 { text-align: left; }
  </style>

.. image:: inc/logo-hpi-no-txt.svg
  :class: understate top right
  :width: 7.5%

.. rst-class:: smaller understate bottom right

* Lukas Pirl
* TCL workshop, November 19, 2021
* Professorship for Operating Systems & Middleware
* Hasso Plattner Institute at the University of Potsdam, Germany


status quo
==========

.. rst-class:: build

* fault-tolerant systems *do* fail

  .. rst-class:: build fade

  * conflicting green in traffic lights
    :understate:`("feindliches Grün")`

  * 8 h Azure outage 2012

    * leap day bug in SSL certificate generation

  * Boeing 737 MAX incidents

    * flight control system relied on non-redundant sensor readings

      .. rst-class:: small

      * :understate:`although FAA identified a lot root causes`

  * 5.5 h Facebook outage 2021

    * wrong manual command took backbone network (i.e., all data centers)
      down

* .. image:: https://www.rbb24.de/content/dam/rbb/rbb/rbb24/2020/2020_01/rbb-reporter/Stoerung.jpg.jpg
    :width: 33%
    :class: top right

* :negative:`but` software systems become
  more :negative:`in numbers`,
  more :negative:`critical` and
  more :negative:`complex`

  * => ensuring :positive:`dependability` is an ongoing, crucial and
    challenging task



=============
dependability
=============

.. rst-class:: large build

* justifiably trust the system to fulfill its specification



dependability
=============

a taxonomy

.. graphviz:: inc/dependability.dot
  :class: bottom right

.. rst-class:: smaller bottom left gray

  * A\. Avižienis, J\.-C\. Laprie, and B\. Randell,
  * "Dependability and Its Threats: A Taxonomy,"
  * in Building the Information Society,
  * Springer, Boston, MA, 2004, pp. 91–120.



dependability attributes
========================

.. graphviz:: inc/dependability-highlight-attributes.dot
  :class: bottom right



dependability threats
=====================

.. graphviz:: inc/dependability-highlight-threats.dot
  :class: bottom right



threats
=======

.. rst-class:: build fade

* fault :gray:`(Fehlerursache)`

  * adjudged or hypothesized error cause

    * in software: bugs/defects

  * might *activate* an error

* error  :gray:`(Fehlerzustand)`

  * incorrect system state
  * might *propagate* to a failure

* failure  :gray:`(Ausfall)`

  * deviation from specification
  * might appear as fault to related systems

.. nextslide::

.. rst-class:: build fade

* single component view

  .. graphviz:: inc/fault-error-failure.dot
    :align: center

* systems of systems view

  .. graphviz:: inc/fault-error-failure-sos.dot
    :align: center



dependability means
===================

.. graphviz:: inc/dependability-highlight-means.dot
  :class: bottom right



dependability evaluation
========================

* two classes of approaches

  .. rst-class:: build fade

  * formal verification

    * prove software :positive:`correct`
    * requires formal specification

      * for all inputs (incl. environment states)
        → **combinatorial explosion**

  * testing

    * prove software :negative:`wrong`
    * discover bugs during runtime
    * requires a fault model



formal verification
-------------------

.. rst-class:: build fade

* increasingly hard

  * increasing complexity

    * higher technology stacks,
      tool chain :gray:`(e.g., compilers)`,
      composition,
      …

  * resource constraints

    * requirements change due to agile development,
      time-to-market pressure,
      …

* attractive for model-driven development

  .. rst-class:: small

  * i.e., model is specification, transformation is formally verified

* usually makes strong assumptions

  .. rst-class:: small

  * e.g., assume correct hardware for verification of ``seL4`` microkernel

* formally verified code might still not meet intentions

  .. rst-class:: small

  * e.g., 802.11i/WPA2 vulnerabilities despite (partial) formal verification



testing
-------

.. rst-class:: build

* widely adopted
* best practice
* extensive

  .. rst-class:: small

  * unit testing,
    integration testing,
    regression testing,
    …

* usually testing "in :positive:`success space`"
* developers/testers might be :negative:`biased`

  .. rst-class:: build

  * tests are expected to succeed

    * code is crafted to to satisfy tests (TDD)
    * xor
    * tests are crafted to test code (non-TDD)





fault injection
===============

* fault injection ⊂ testing ¹

.. rst-class:: build fade

* experimental dependability assessment

  * idea: lower complexity

    * compared, e.g., to formal verification

  .. image:: inc/pull-plug.png
    :width: 25%
    :class: bottom right

* concept

  #. forcefully activate :gray:`(i.e., "inject")` faults

     * or, forcefully introduce errors

  #. assess delivered quality of service

.. rst-class:: smaller gray bottom

¹ no widely-accepted definition to differentiate between the two



:underline:`software` fault injection
=====================================

.. rst-class:: build

* implemented in software *and* targeting software

  * != hardware-implemented fault injection (HWIFI)

    * targeting hardware, e.g., exposition to increased radiation

  * != software-implemented fault injection (SWIFI)

    * targeting hardware, e.g., flipping of bits in memory

* requires

  * faultload

    * :highlight1:`which` faults (from :highlight1:`fault model`)
      to inject :highlight2:`when` and :highlight2:`where`
      (depends on :highlight2:`operational profile`)

  .. rst-class:: build

  * workload

    * for realistic fault activation and error propagation



typical objectives
==================

.. rst-class:: build fade

* find "dependability bottlenecks" / single points of failure
* assess quality of service in presence of faults

  * performance degradation

    * e.g., bandwidth, latency

  * dependability attributes

    * availability, reliability, safety, security, integrity, maintainability

* assess specific fault tolerance mechanisms

  * e.g., efficiency, effectiveness

* determine coverage of error detection and recovery



typical "meta objectives"
-------------------------

.. rst-class:: build fade

* experiences & confidence regarding dependability

  * e.g., developers, testers, operators, architects, best-practices, documentation

* bug fixes for fault tolerance mechanisms
* well-tested and -understood fault tolerance mechanisms
* **measurements**

  * only objective measures allow comparisons between different systems

    * thus, allow to judge improvement/worsening between different versions



fault model
===========

* set of faults assumed to occur

  .. rst-class:: build fade

  * hardware faults

    * relatively established fault model

      * bit flips:
        :gray:`single xor multi`

      * stuck-at faults:
        :gray:`a bit permanently set to 1 (stuck-at-1) xor 0 (stuck-at-0)`

      * bridging faults:
        :gray:`two signals are connected although they shouldn't be`

      * delay faults:
        :gray:`delay of a path exceeds clock frequency`

  * software faults

    * fault models not as established

      * stop, crash, omission, timing, …

    .. graphviz:: inc/fault-model-hierarchy.dot
      :class: bottom right

    .. rst-class:: ref bottom left

      * Polze A, Schwarz J, Malek M. Automatic generation of
        fault-tolerant CORBA-services. InProceedings.
      * 34th International Conference on Technology of Object-Oriented
        Languages and Systems. TOOLS 34 2000 Aug 4 (pp. 205-213). IEEE.



injection trigger
=================

.. rst-class:: build

* time-based

  .. rst-class:: build

  * absolute xor relative

    * e.g., absolute time of day, relative to run time

  * one-time vs. periodic vs. sporadic

    * e.g., fixed rate, between a minimum and a maximum rate

* location-based

  * depends on system under consideration and level of abstraction

    * e.g., on access of specific memory areas,  specific nodes

* execution-driven

  * based on control flow during runtime



target artefact
===============

.. rst-class:: build fade

* source code

  * e.g., change control flow, add sleeps

* intermediate code representation

  * e.g., change operators or constants in bytecode

* binary representation

  * e.g., bit flips

* state

  * e.g., memory/storage modifications, edge-case states of environment

* :underline:`environment` :understate:`(focus for testbeds?)`

  * e.g., clock drift, node crashes, misbehaving hardware, related APIs'
    behaviour


.. slide:: example injection targets for applications
  :level: 3

  .. rst-class:: build

  * **black box**:
    :positive:`less` intrusiveness,
    :positive:`less` interference with result,
    :positive:`less` coupling,
    …

    .. graphviz:: inc/fi-blackbox.dot
      :align: center

  * **white box**:
    possibly :positive:`more` insights,
    :positive:`higher` performance,
    :positive:`easier` to debug,
    …

    .. graphviz:: inc/fi-whitebox.dot
      :align: center



black vs. white box
-------------------

different approaches have different advantages and disadvantages, e.g.:

================ ============ =============== ============ ===============
\                        Hardware                     Software
---------------- ---------------------------- ----------------------------
\                with contact without contact with contact without contact
================ ============ =============== ============ ===============
cost              high        high            low           low
perturbation      none        none            low           high
risk of damage    high        low             none          none
time resolution   high        high            high          low
injection points  chip pin    chip internal   memory        memory
\                                             software      IO controller
controllability   high        low             high          high
trigger           yes         no              yes           yes
repeatability     high        low             high          high
================ ============ =============== ============ ===============

.. rst-class:: ref

M.-C. Hsueh, T. K. Tsai, and R. K. Iyer, "Fault injection techniques and tools," vol. 30, no. 4, pp. 75–82, Apr. 1997.



Rail2X
======

.. image:: inc/logo-rail2x.svg
  :class: logo-rail2x

.. image:: inc/logo-mfund.png
  :class: logo-mfund

.. rst-class:: build

* Vehicle-to-X communication (V2X) for railways (Rail2X)

  * smart mobility through interconnection of vehicles & infrastructure
  * analogue: Car2X, Ship2X, Airplane2X

* V2X :gray:`(hence, Rail2X)` uses specialized WiFi standard IEEE 802.11p

  * pre-defined messages :gray:`(e.g., emergency vehicle alert)`
  * focus on low latency :gray:`(compared to, e.g., 802.11n)`
  * no access points :gray:`(similar to ad-hoc mode)`
  * higher range :gray:`(up to 1 km)`

.. rst-class:: logo-banner

  * .. image:: inc/logo-db.svg
  * .. image:: inc/logo-siemens.svg
  * .. image:: inc/logo-dlr.svg
  * .. image:: inc/logo-hpi-no-txt.svg
  * .. image:: inc/logo-dralle.png



Rail2X in the IoT Lab
---------------------

.. rst-class:: build fade

* .. graphviz:: inc/rail2x-use-case-1.dot
    :class: left

* .. graphviz:: inc/rail2x-use-case-1-jamming.dot
    :class: right

* .. graphviz:: inc/802.11p-jamming.dot
    :class: bottom right

* .. image:: inc/el20-no-bkg.png
    :class: bottom left
    :width: 25%

.. image:: inc/logo-iot-lab.svg
  :width: 10%
  :class: top right



IEEE 802.11p packet round trip times while jamming
--------------------------------------------------

* .. image:: inc/richter18performance-figure5.svg
    :class: invert
    :width: 100%

* packet round trip times are lowest using IEEE 802.11p OCB mode

  * esp. with lowest standard deviation

    * :understate:`desirable for soft real-time applications`

.. rst-class:: ref left

  Richter D, Pirl L, Beilharz J, Werling C, Polze A. Performance of
  Real-TimeWireless Communication for Railway Environments with IEEE
  802.11p.



bigger picture: processes and methodologies
===========================================

* FIDD: fault-injection-driven development

  * incorporate fault injection in development practices

    * | in analogy and in addition to test-driven development

* .. image:: inc/fidd-three-environments.svg
    :width: 90%
    :align: center

.. rst-class:: ref right

  Dr. Lena Feinbube



success stories
===============

  .. rst-class:: build

* ISO 26262 :gray:`(Road vehicles – Functional safety)`
  recommends fault injection

* Linux kernel

  * e.g., through syscall fuzzing
* software fault injection in production

  .. rst-class:: build

  * Netflix: Chaos Monkey

    * terminates AWS EC2 instances

      * in AWS Auto Scaling Groups

    * during business hours only

      * staff is watching and can react quickly

  * Etsy :gray:`(e-commerce)`

* *chaos engineering* offered as a service by major Cloud providers


.. rst-class:: no-footer no-slide-no

software fault injection – conclusion
=====================================

.. raw:: html

  <style>
  .dependability-taxonomy-conclusion {
  width: 40%;
  top: 17.5%;
  }
  </style>

.. rst-class:: build

* mind dependability as a :positive:`whole`

  .. graphviz:: inc/dependability.dot
    :class: top right dependability-taxonomy-conclusion

* fault injection as "first class citizen" in development

  * continuous
  * automated
  * scoped

    .. rst-class:: build

    * **Where** to inject? → dependability model

      .. rst-class:: small

      * know which faults *should* be tolerated (injecting non-tolerated
        faults is usually pointless)

    * **What** to inject? → fault model

      .. rst-class:: small

      * bug trackers, vulnerability databases and failure reports can
        give inspiration

    * **When** to inject? → trigger

      .. rst-class:: small

      * likely to be chosen according to workload, when injecting during
        runtime

* .. rst-class:: smallest bottom right understate

    * Lukas Pirl
    * TCL workshop, November 19, 2021
    * Professorship for Operating Systems & Middleware
    * Hasso Plattner Institute at the University of Potsdam, Germany
