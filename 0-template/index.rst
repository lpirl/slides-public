.. include:: projector-and-presenter-test-slides.rst

=====
title
=====

.. rst-class:: smaller

* Lukas Pirl, …, and Andreas Polze
* lorem ipsum event
* Professorship for Operating Systems and Middleware
* Hasso Plattner Institute at the University of Potsdam, Germany

first slide
===========

.. rst-class:: build fade

* foo
* bar
* baz
