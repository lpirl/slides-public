This is a template `hieroglyph <http://hieroglyph.io>`__ presentation.

It uses the ``simple-slides-dark``
`theme <https://github.com/lpirl/hieroglyph-themes>`__.

To get this presentation going

.. code-block:: shell

  # 0. create/activate a virtualenv, if you want to
  # 1. install dependencies, update submodules, etc.
  $ make prepare
  # 2. start a local Web server (will build slides automatically)
  $ make serve
  # 3. point your browser to http://127.0.0.1:8000
