# This makefile mainly delegates to the Makefiles of the presentation
# directories.
# Make sure that goals are made in the fashion:
#   prepare slides A, build slides A, prepare slides B, build slides B
# instead of:
#   prepare slides A, prepare slides B, build slides A, build slides B
# because the latter could cause problems e.g. with the installed
# dependencies.

all: slides

# Delegate all uncaught targets to the Makefiles of the slide
# directories:
.DEFAULT:
	find -name conf.py -printf '%h\0' | xargs -t0 -I{} \
 		$(MAKE) $(MAKEFLAGS) -C "{}" $(MAKECMDGOALS)
