Place files here, which should be identical for all presentations.

In particular, presentations should not be required to be updated in
case the files in this directory get updated.

This is, e.g., true for:

* generic first slides to test the projector and presenter
* generic last slides with a pointer to the slides repository
* a favicon

This is, e.g., not true for:

* images/figures
* configuration files
