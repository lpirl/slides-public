.. note:

  These two slides serve two purposes:
  * the circles in the corners help to identify if the whole slide area
    is visible through the projector;
  * since it is two slides, one can check if advancing to the next slide
    with the presenter (the handheld input device) works.
    Also this is a nice blackout while you're waiting with your
    presentation to begin.

.. slide::
  :class: projector-test

  .. image:: inc/circle-16x6x1.svg
    :class: top left

  .. image:: inc/circle-16x6x1.svg
    :class: top right

  .. image:: inc/circle-16x6x1.svg
    :class: bottom left

  .. image:: inc/circle-16x6x1.svg
    :class: bottom right

.. slide::
  :class: blackout
