.. include:: projector-and-presenter-test-slides.rst

========================
prepare for the blackout
========================

.. rst-class:: large right

  software fault injection for dependability

.. image:: inc/logo-hpi.svg
  :class: bottom right white-background-se hpi-logo

.. rst-class:: smaller bottom left

* Lukas Pirl, Lena Feinbube, and Andreas Polze
* HPI Future SOC Lab Day, Fall 2018
* 14.11.2018
* Operating Systems & Middleware Group
* Hasso Plattner Institute at the University of Potsdam, Germany

agenda
======

.. plot:: inc/agenda.py
  :class: invert
  :width: 55%
  :align: center

power outages in the HPI Future SOC Lab: August 2018
====================================================

.. rst-class:: build fade

* :positive:`announced` power outage

  * due to maintenance of power supply infrastructure

* :negative:`failed emergency power supply`

  * due to worn batteries

    * virtually no capacity left
    * no monitoring mechanism

* :negative:`storage system failed to boot`

  * due to too many failed disks for dependable r/w mode

    * bad design in the hardware log component prevented error reporting

power outages in the HPI Future SOC Lab: August 2018 (cont.)
============================================================

.. rst-class:: build fade

* core services suffered from :negative:`hardware failures`

  * due to damaged system boards by power outage
    :gray:`(or power peak?)`

    .. rst-class:: small

    * machines were never turned off before

* :negative:`staff partly unavailable`

  * due to semester break


power outages in the HPI Future SOC Lab: October 2018
=====================================================

.. rst-class:: build fade

* :negative:`unannounced` power outage

  * due to a maintenance failure at transformer station (supplier)

* :negative:`failed emergency power supply`

  * due to bad luck … batteries were removed for replacement at that very
    moment

* :positive:`most services recovered automatically`

  * e.g., core services, storage
  * minor manual interference needed

    .. rst-class:: gray

    * due to, e.g.,
      dependencies between services,
      previously unobserved transient failures in boot processes

* :positive:`no hardware failures`
* :positive:`staff in standby`

power outages in the HPI Future SOC Lab: summary
================================================

.. rst-class:: build

* actions taken

  .. rst-class:: build fade

  * :positive:`replaced worn hardware`

    * e.g., disks, batteries, whole machines

  * :positive:`improved robustness of software stack`
  * :positive:`practiced processes`

* observations

  .. rst-class:: build fade

  * :positive:`less negative impact` cause by the second power outage
  * :positive:`less negative impact` when staff is in standby

* → in retrospective, the failures made the infrastructure more dependable
* **→ Why not regularly provoke failures in a controlled and deliberate
  manner?**

fault injection
===============

.. rst-class:: build fade

* fault injection ⊂ testing ¹

  .. rst-class:: top right

    * .. graphviz:: inc/fault-injection-subset-testing.dot

* experimental dependability assessment

  * mental model rooted in failure space

    * i.e., "What can go :negative:`wrong`?" instead of
      "What needs to :positive:`work`?"
    * as opposed to testing :gray:`(usually)`

  * promises lower complexity

    * compared to, e.g., formal verification

* *software* fault injection

  * injection implemented in software *and* targeting software

.. rst-class:: smaller gray bottom

¹ no widely-accepted definition to differentiate between the two

.. nextslide::
  :increment:

* concept

  .. rst-class:: build fade

  #. forcefully :negative:`activate suspected causes for failure`

     * i.e., forcefully "inject" "faults" or "errors"

  #. :highlight1:`assess` delivered :highlight1:`quality of service`

     * e.g., performance, availability, reliability, security, integrity

  #. :positive:`leverage insights`

     * understand dependability properties
     * gain experiences for operation and development
     * compare with dependability requirements
     * improve the service
     * …

.. image:: inc/pull-plug.png
  :width: 20%
  :class: right bottom

software fault injection: example
=================================

.. rst-class:: build

* .. image:: inc/fault-tree-example.svg
    :width: 42%
    :class: bottom right invert

.. ~ * scenario

.. ~   * Web site
.. ~   * dual-modular load balancer *and* Web server

.. ~     * :gray:`i.e., one failed device each can be tolerated`

.. rst-class:: build fade

* possible :negative:`faults` to inject :gray:`which should be tolerated`

  * crash one of the load balancers
  * crash one of the Web servers
  * crash one each :gray:`(i.e., maximum fault load)`
  * freeze one of the load balancers
  * …

* possible :highlight1:`measurements` :gray:`to judge quality of service`

  * latency and bandwidth of requests
  * latency of failover and failback
  * …

dependability – the bigger picture
==================================

.. rst-class:: build fade

* dependability: *a* measure for software quality

* software services grow in :highlight1:`numbers` and :highlight1:`complexity`

  * :gray:`especially distributed systems`

* software services are increasingly :negative:`critical`

* → ensuring dependability hence increasingly important
  and challenging alike

* success stories demonstrate the :positive:`potential` of
  fault injection to increase dependability

  * e.g., in software development and operation
  * long-established in hardware development
  * less widely adopted in the software domain

agenda
======

.. plot:: inc/agenda.py
  :class: invert
  :width: 55%
  :align: center

==========
case study
==========

.. rst-class:: larger right

  * dependability assessment of OpenStack

OpenStack
=========

.. image:: inc/logo-openstack.svg
  :width: 15%
  :class: right

.. rst-class:: build fade

* framework for building IaaS platforms

  * providing, e.g., VMs, storage, networking to its users

* free, open-source software

  .. rst-class:: gray

  * Apache License 2.0

* emerged from projects at RackSpace and NASA

  .. rst-class:: gray

  * Cloud Files and OpenNebula

* composed of services

  * external communication mostly via RESTful HTTP APIs
  * internal communication via message queues and RESTful HTTP APIs

OpenStack main services
=======================

to give an idea about OpenStack services, a selection:

.. rst-class:: bottom right

  * .. graphviz:: inc/openstack-services.dot

Fuel
====

.. image:: https://cdn.mirantis.com/wp-content/uploads/2015/09/fuel-for-openstack-logo.png
  :class: bottom right white-background-se fuel-logo
  :width: 10%

.. rst-class:: build fade

* OpenStack distribution
* UIs to manage OpenStack instances
* capable of installing a high availability setup

  * :gray:`high availability :≈ as commonly used`

    * :gray:`i.e., higher dependability than common, comparable systems`

* places OpenStack services on three different node roles

  * **controller:**
    set of most OpenStack services, :positive:`supports redundancy`

  * **compute:**
    executes virtual machines, :negative:`no redundancy`

  * **storage:**
    storage for user data, :positive:`usually redundant`

OpenStack Fuel node types and networks
======================================

.. rst-class:: build fade

* .. rst-class:: bottom right

  * .. graphviz:: inc/fuel-nodes-and-networks.dot

* | How to run this setup
  | virtualized in the FSOC Lab?

setup in Future SOC Lab
=======================

.. rst-class:: build fade

* after a lot of experimentation

  * with many different configurations
  * on different hardware

* 5 x HPE ProLiant m710p server cartridge


  * .. image:: inc/hpe-m710.png
      :class: top right
      :width: 25%
  * Intel Xeon E3-1284L v4, 4C/8T, **2.90GHz**
  * **32GB**, 4 × 8GB PC3L-12800 (**DDR3**–1600) SODIMM
  * 120GB HP 765479-B21 SSD (M.2 2280)
  * Mellanox Connect-X3 Pro (dual **10GbE**)
  * Linux 4.4.0 (x86_64)
  * Ubuntu 16.04

setup (cont.)
=============

.. rst-class:: bottom right

  * .. graphviz:: inc/dfuel.dot

the virtualized Fuel setup still looks like this
================================================

.. rst-class:: build fade

* .. rst-class:: bottom right

  * .. graphviz:: inc/fuel-nodes-and-networks.dot

* | Which node failures
  | can be tolerated?

dependability model
===================

.. rst-class:: build fade

* dependability models help

  * designing services
  * understanding services

  .. rst-class:: highlight1

  * **Guide fault injection campaigns?**

    * structured identification of scenarios
    * programmatic exercise of scenarios
    * for reproducibility

* .. image:: inc/fuel-fault-tree-simplified.svg
    :class: right top invert
    :width: 58%

* workload

  * distributed storage service

    * i.e., VMs on OpenStack Fuel

  * write latency is quality criterion

automation
==========

.. image:: inc/faultmill-simplified-pretty.svg
  :class: bottom right
  :width: 98%

.. nextslide::

.. image:: inc/faultmill-pretty.svg
  :class: bottom right
  :width: 98%

campaign derivation
===================

.. rst-class:: build

* maximize fault load → *dependability stress*

  * i.e., inject as many faults as simultaneously tolerable

* campaign derivation from dependability model

  .. rst-class:: build

  * .. image:: inc/fuel-fault-tree-simplified.svg
      :class: right top invert
      :width: 30%

  .. rst-class:: build

  0. identify all injection points

     * i.e., all basic events in fault tree

     * .. rst-class:: bottom right invert

         * .. graphviz:: inc/scenarios-funnel.dot

  #. calculate all possible scenarios

     * i.e., power set of all injection points

  #. remove non-tolerated scenarios

     * i.e., scenarios containing a *mincut*

  #. remove non-maximal scenarios

     * i.e., scenarios contained by others

scenarios
=========

.. image:: inc/scenarios.svg
  :class: bottom right
  :width: 52%

.. rst-class:: build fade

* | ``11`` basic events
  | ``2048`` possible scenarios
  | ``36`` "maximized" scenarios
  |
* | **< 2% of all scenarios to exercise**
  |
* even more "savings" considering …

  * … multiple runs per scenario
  * … multiple fault types

exercise of campaigns
=====================

.. rst-class:: build fade

* programmatic exercise of campaign

  * coordinated by an custom tool

    .. rst-class:: gray

    * which also derives the campaign from the model

  * via user-provided executables

    .. rst-class:: gray

    * for actual interaction with system under test
    * intentionally low-level of integration

      * easy to adapt

  .. rst-class:: bottom right invert

    * .. graphviz:: inc/faultmill-workers-simplified.dot

* automation is a lot of work and fiddling

  * esp. for complex distributed systems
  * but not really exciting


results
=======

.. image:: inc/scenarios-results.svg
  :class: bottom right
  :width: 69%

agenda
======

.. plot:: inc/agenda.py
  :class: invert
  :width: 55%
  :align: center

recap
=====

.. rst-class:: build fade

  * :gray:`involuntary` case study: power outages in the HPI Future SOC Lab

    * actually :positive:`enhanced` the dependability
      :gray:`… in the long run ;)`

* |

.. rst-class:: build fade

  * → confrontation with :negative:`faults`, :negative:`errors` and
    :negative:`failures` can improve :positive:`dependability`

    * technical :gray:`(e.g., fault tolerance mechanism)`
      and organizational :gray:`(e.g., processes)`

* |

.. rst-class:: build fade

  * **software fault injection** for dependability assessments

    * guided by dependability model
      :gray:`for automation and reproducibility`

    * *dependability stress*
      :gray:`to provoke synergistic failures and increases efficiency`

Questions?
==========

.. rst-class:: invert

  * .. graphviz:: inc/scenarios-funnel.dot

* .. image:: inc/faultmill-pretty.svg
    :class: top right
    :width: 50%

.. rst-class:: bottom left

  * .. graphviz:: inc/dfuel.dot

* .. image:: inc/scenarios-results.svg
    :class: bottom right
    :width: 42%


.. =====================================================================

.. slide::

======
backup
======

top-notch fault-tolerant services fail as well
==============================================

* 2.5h Facebook outage 2010

   * "friendly" DDoS due to wrong configuration value

* 8h Azure outage 2012

   * leap day bug in SSL certificate generation

* 4.5h Amazon S3 outage 2017

   * typo in manual command took “too many” servers down

fundamental questions for fault injection assessments
=====================================================

* very specific to the service under test
* **What** to inject? → fault model

  .. rst-class:: gray

  * fault classes, e.g., crash faults

* **When** to inject? → trigger

  .. rst-class:: gray

  * when injecting during runtime, likely to be chosen according to workload

* **Where** to inject? → dependability model

  .. rst-class:: gray

  * at spots where faults *should* be tolerated

* have a clear **scope**

full dependability model
========================

.. image:: inc/fuel-fault-tree.svg
  :class: right top invert
  :width: 42%
