#!/usr/bin/env python

import matplotlib
import matplotlib.pyplot as plt
from os.path import basename

if 'lpirl-light-slides' in plt.style.available:
  plt.style.use(['classic', 'lpirl-light-slides'])
else:
  print('WARNING: style "lpirl-light-slides" not found')

# since this is a title-like plot
plt.rcParams.update({'font.size': 24})

data = (
  # time, label, level of abstraction
  (.75, .75, "power\noutages"),
  (1.5, 2.0, "software fault\ninjection"),
  (3.0, 2.5, "dependability"),
  (4.5, 1.5, "case\nstudy"),
)

times = tuple(d[0] for d in data)
levels = tuple(d[1] for d in data)

plt.xticks([])
plt.yticks([])

plt.gca().set_xlim([0, max(times)])
plt.gca().set_ylim([0, max(levels)])


# hide axes lines
for side in ['right','top']:
    plt.gca().spines[side].set_visible(False)

# add labels
for d in data:
  plt.text(
    d[0],
    d[1],
    d[2],
    horizontalalignment='center',
    verticalalignment='center',
    backgroundcolor='white'
)

plt.plot(times, levels)

plt.xlabel('duration of this talk')
plt.ylabel('level of abstraction')

plt.show()
