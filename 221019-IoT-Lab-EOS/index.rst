.. rst-class:: no-footer no-slide-no middle-title no-title

===========
HPI IoT lab
===========

|

.. image:: inc/logo-iot-lab.svg
  :align: center
  :width: 25%

.. rst-class:: bottom small

* Lukas Pirl
* Introduction of IoT Lab in Lecture on Embedded Operating Systems
* Professorship for Operating Systems and Middleware
  of Prof. Andreas Polze
* Hasso Plattner Institute, University of Potsdam



HPI IoT lab
===========


.. image:: inc/lab-w.jpeg
  :class: top left background
  :width: 100%



.. nextslide::


.. image:: inc/lab-e.jpeg
  :class: top left background
  :width: 100%



.. nextslide::


.. image:: inc/lab-workshop.jpeg
  :class: top left background
  :width: 100%



workshop
========

* general tools
* soldering
* electronics components
* bulk & DIY cables, connectors, etc.
* power supply
* measurement
* …

  * .. image:: inc/nordic-power-profiler.webp
      :width: 20%

.. image:: inc/ea-ps-5200-10a.png
  :class: top right
  :width: 33%

.. image:: https://www.rigol.eu/Public/Uploads/uploadfile/images/20190827/20190827131835_5d64bd2bd26e6.png
  :class: bottom right
  :width: 45%



Carrera slot car
================

|
|
|
|
|
|

* self-driving slot car
* gyroscope, Lidar, …

.. image:: inc/carrera-self-driving.jpeg
  :class: top left background
  :width: 100%



.. nextslide::


* custom PCBs

.. image:: inc/carrera-self-driving-21.jpeg
  :class: top left background
  :width: 100%

.. rst-class:: smaller bottom left

  * Björn Daase
  * Leon Matthes



.. nextslide::


* .. image:: inc/daase21observig-figure3f.png
   :class: right invert
   :width: 40%

.. rst-class:: build

* challenges


  * unusual power supply characteristics
  * remote/mobile debugging

* PCB design

  .. image:: inc/carrera-self-driving-21r3.svg
    :width: 30%

.. rst-class:: ref

  Daase, B., Matthes, L., Pirl, L., & Wenzel, L. Observing a Moving
  Target – Reliable Transmission of Debug Logs from Embedded Mobile
  Devices.



Carrera sensor & actuator board
===============================

.. image:: inc/carrera-board.jpg
  :class: right background
  :width: 50%

* working with the digital protocol
* firmware level
* e.g., prevent overtaking the safety car



single-board computers
======================

* distributed computing
* energy-aware computing
* heterogeneous computing

  * (GP)GPU, big.LITTLE, …

.. image:: inc/cabinet.jpeg
  :class: top right background
  :width: 42%

.. rst-class:: bottom small

  `list of experiment hardware
  <https://osm.hpi.de/iot-lab/docs/hardware/generated-overview-of-experiment-hardware-list.html>`__

  .. rst-class:: smallest

    https://osm.hpi.de/iot-lab/docs/hardware/generated-overview-of-experiment-hardware-list.html

digital rail
============


switch point machine

.. image:: inc/point-machine.jpg
  :class: bottom center background
  :width: 100%



.. nextslide::


color light signal (Ks)

.. image:: inc/ks.jpeg
  :class: bottom center background
  :width: 30%



.. nextslide::


.. image:: inc/frauscher-field1.jpeg
  :class: bottom left background
  :width: 110%



.. nextslide::


.. image:: inc/frauscher-field2.jpeg
  :class: bottom center background
  :width: 30%



.. nextslide::


.. rst-class:: bottom

  model-generated code counts axles and controls signal

.. image:: inc/frauscher-field3.jpeg
  :class: top left background
  :width: 100%



.. nextslide::

* * * * .. image:: inc/logo-drss.svg
          :width: 55%

.. rst-class:: bottom right

  * https://hpi.de/drss
  * DRSS 2022 already done, join 2023! :)


.. nextslide::


* automation of proprietary devices
* e.g., axle counter object controller

.. image:: inc/frauscher-lab.jpeg
  :class: bottom left background
  :width: 100%



testbed automation
==================

.. image:: inc/network-sd-card.svg
  :class: bottom right background invert
  :width: 66%

.. rst-class:: ref left

  | Schröter, V., Boockmeyer, A., & Pirl, L.
  | NetSD: Remote Access to Integrated
  | SD Cards of Embedded Devices.



.. nextslide::


.. image:: inc/network-sd-card.png
  :class: bottom right background
  :width: 80%


.. rst-class:: ref left

  | Valentin Schröter
  | Tobias Zagorni



.. nextslide::

* .. image:: inc/diez-433rx.jpg
    :width: 25%
    :align: right

* reverse engineering of switchable power sockets

* .. image:: inc/diez-433tx.jpg
    :width: 25%
    :align: right

* .. image:: inc/diez-socket.jpg
    :width: 33%

* .. image:: inc/diez-signal.jpg
    :width: 100%
    :class: bottom

.. rst-class:: ref top

  Maximilian Diez



embedded devices & OS
=====================

.. image:: https://www.seekpng.com/png/full/207-2079825_arduino-nano-v-atmega-p-bits-lt-arduino.png
  :class: right top
  :width: 25%

.. image:: https://joy-it.net/files/files/Produkte/SBC-NodeMCU-ESP32/SBC-NodeMCU-ESP32-01.png
  :class: right bottom
  :width: 25%

* devices

  * Arduino
  * ESP32
  * Beckhoff SPS
  * Lego
  * Fischertechnik
  * … *open to suggestions!* :)

    * incl. designing own PCBs

* operating systems

  * RIOT, \*RTOS, OpenWRT, embedded Linux, Contiki, Android, …



radio technology
================


.. rst-class:: bottom

  * LoRa-WAN

.. image:: inc/lora.jpeg
  :class: top left background
  :width: 100%


.. nextslide::

.. rst-class:: bottom right

  SDR

.. image:: inc/USRP-B210.png
  :class: top left background
  :width: 100%


.. nextslide::

.. rst-class:: bottom right

  IEEE 802.11p

.. image:: inc/11p.jpg
  :class: top left background
  :width: 100%



Rail2X
======

.. image:: inc/logo-rail2x.svg
  :class: logo-rail2x

.. image:: inc/logo-mfund.png
  :class: logo-mfund

.. rst-class:: build

* Vehicle-to-X communication (V2X) for railways (Rail2X)

  * smart mobility through interconnection of vehicles & infrastructure
  * analogue: Car2X, Ship2X, Airplane2X

* V2X :gray:`(hence, Rail2X)` uses specialized WiFi standard IEEE 802.11p

  * pre-defined messages :gray:`(e.g., emergency vehicle alert)`
  * focus on low latency :gray:`(compared to, e.g., 802.11n)`
  * no access points :gray:`(similar to ad-hoc mode)`
  * higher range :gray:`(up to 1 km)`

.. rst-class:: logo-banner

  * .. image:: inc/logo-db.svg
  * .. image:: inc/logo-siemens.svg
  * .. image:: inc/logo-dlr.svg
  * .. image:: inc/logo-hpi-no-txt.svg
  * .. image:: inc/logo-dralle.png



Rail2X in the IoT Lab
---------------------

.. rst-class:: build fade

* .. graphviz:: inc/rail2x-use-case-1.dot
    :class: left

* .. graphviz:: inc/rail2x-use-case-1-jamming.dot
    :class: right

* .. graphviz:: inc/802.11p-jamming.dot
    :class: bottom right

* .. image:: inc/el20-no-bkg.png
    :class: bottom left
    :width: 25%



IEEE 802.11p packet round trip times while jamming
--------------------------------------------------


* .. image:: inc/richter18performance-figure5.svg
    :class: invert
    :width: 100%

* packet round trip times are lowest using IEEE 802.11p OCB mode

  * esp. with lowest standard deviation

    * :understate:`desirable for soft real-time applications`

.. rst-class:: ref left

  Richter D, Pirl L, Beilharz J, Werling C, Polze A. Performance of
  Real-TimeWireless Communication for Railway Environments with IEEE
  802.11p.



fault injection
===============

.. rst-class:: build

* experimental dependability assessment

  * promises lower complexity

    * compared, e.g., to formal verification

* fault injection ⊂ testing
* try to prove the system :negative:`wrong`

  * instead of trying to prove the system :positive:`correct`
    :gray:`as with "traditional" testing`

  * counter developers' bias towards happy cases

* concept

  #. forcefully activate :gray:`("inject")` suspected error causes
     :gray:`("faults")`

     .. image:: inc/pull-plug.png
       :width: 20%
       :class: bottom right

  #. assess delivered quality of service



development of methodologies
============================

fault-injection-driven development

.. image:: inc/fidd-three-environments.svg
  :width: 95%
  :align: center

.. rst-class:: ref right

  Dr. Lena Feinbube



.. nextslide::


.. image:: inc/hybrid-co-simulation.svg
  :width: 80%
  :align: center



hybrid testbeds & co-simulation
===============================

* hybrid: software, hardware, and models "in-the-loop"

  * e.g., simulated wireless & physical wireless network

* co-simulation: coupling of multiple domain-specific simulations

  * e.g., *SUMO* for traffic & *ns-3* for networking

* https://github.com/diselab/marvis

.. image:: inc/marvis-figure1.svg
  :class: bottom right invert
  :width: 45%

.. rst-class:: no-footer no-slide-no



distributed testbeds
====================

* EULYNX live lab: distributed test environment

  * Kubernetes + Akri, generated interlocking software, automated tests, …

.. graphviz:: inc/eulynx-live.dot
  :class: bottom



HPI IoT lab
===========

.. image:: inc/logo-iot-lab.svg
  :class: right
  :width: 15%

*… an environment for prototyping and assessments*

.. rst-class:: build fade

* building, DIY, testing, …
* hybrid, co-simulated, and distributed setups
* across/coupling of different domains
* model-driven and -supported approaches
* alternatives to specialized (test) hardware
* hardware, software and model "in the loop"
* dependability

  * :gray:`(e.g., through fault injection)`

* .. rst-class:: bottom right smaller gray text-right

    * Lukas Pirl
    * Introduction IoT Lab in Lecture on Embedded Operating Systems
    * Professorship for Operating Systems and Middleware
    * Hasso Plattner Institute, University of Potsdam
