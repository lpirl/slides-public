Here, I keep (the sources of) my (non-confidential) slides.

* `browse built slides <https://lpirl.gitlab.io/slides-public>`__
* `download built slides <https://gitlab.com/lpirl/slides-public/builds/artifacts/master/download?job=pages>`__

The presentations herein are based on `hieroglyph <http://hieroglyph.io>`__
together with the `theme "simple-slides-dark"
<https://github.com/lpirl/hieroglyph-themes>`__.

To get a presentation going:

.. code-block:: shell

  # 0. create/activate a virtualenv, if you want to
  # 1. install dependencies, check out submodules
  $ make prepare
  # 2. start a local Web server (will build slides automatically)
  $ make serve
  # 3. point your browser to http://127.0.0.1:8000

As long as
`there are pull request by myself for the official hieroglyph repository <https://github.com/nyergler/hieroglyph/pulls/lpirl>`__ you may want to
try `my fork of hieroglyph with all those requested changes merged <https://github.com/lpirl/hieroglyph>`__, in case you run into troubles.

------------------------------------------------------------------------
