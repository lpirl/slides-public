sender =  DockerNode('sender', docker_build_dir='./sender')
converter = DockerNode('converter', docker_build_dir='./converter')
receiver = DockerNode('receiver', docker_build_dir='./receiver',
                      exposed_ports={1883:1884})

v2x_net =  Network('10.0.1.0', '255.255.255.0')
v2x_channel = v2x_net.create_channel(channel_type.WiFiChannel)
v2x_channel.connect(sender, ifname='i_v2x')
v2x_channel.connect(converter, ifname='i_conv_v2x')

wan_net = Network('10.0.2.0', '255.255.255.0')
wan_channel = wan_net.create_channel(delay='10ms', channel_type=CSMAChannel)
wan_channel.connect(converter, ifname='i_conv_wan')
wan_channel.connect(receiver, ifname='i_wan')

scenario = Scenario()
scenario.add_network(v2x_net)
scenario.add_network(wan_net)

with scenario as simulation:
  simulation.simulate(simulation_time=30)
