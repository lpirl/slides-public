#!/usr/bin/env python3
# -*- coding: utf-8 -*-

extensions = ['hieroglyph']

# required, so that serving the slides under "/" works in the browser
master_doc = 'index'

# what to exclude when looking for source files
exclude_patterns = [
    '.DS_Store',
    'Thumbs.db',
    'README*',
    '**/README*',
    'glossary/*',
    'build',
    'inc',
]

# required, so that the HTML title does not get suffixed with
# "… documentation"
html_title = ""

html_scaled_image_link = False

# those files are copied to the output directory, no matter if in use
html_static_path = ['static']

rst_prolog = '.. include:: prolog.rst'
rst_epilog = '.. include:: ../0-generic/epilog.rst'

slide_theme_path = ['themes']
slide_theme = 'simple-slides-dark'
slide_footer = '　'.join((
    'DiAK: Das digitale Andreaskreuz',
    'Digital Rail Summer School 2022',
    'lukas.pirl@hpi.de'
))
slide_numbers = True

# Remember that the directories where the files listed here in must be
# in ``html_static_path``.
slide_theme_options = {'custom_css': 'custom.css'}

pygments_style = 'monokai'

# graphviz support
# ================

extensions.append('sphinx.ext.graphviz')
graphviz_output_format = 'svg'
graphviz_dot_args = [
    '-Gfontname=Ubuntu,sans',
    '-Efontname=Ubuntu,sans',
    '-Nfontname=Ubuntu,sans',
]

# matplotlib support
# =================

# ~ try:
    # ~ from matplotlib.sphinxext.plot_directive import TEMPLATE as PLOT_TEMPLATE
# ~ except ImportError:
    # ~ pass
# ~ else:
    # ~ extensions.append('matplotlib.sphinxext.plot_directive')
    # ~ plot_formats = ['svg', 'png']
    # ~ plot_include_source = False
    # ~ plot_html_show_formats = False
    # ~ plot_html_show_source_link = False

# imgmath support
# ===============

# ~ extensions.append('sphinx.ext.imgmath')
# ~ imgmath_image_format = 'svg'
