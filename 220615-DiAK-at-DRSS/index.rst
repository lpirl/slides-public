.. rst-class:: hidden not-in-print

===============================
DiAK: Das digitale Andreaskreuz
===============================


Historisch: Rail2X
==================

.. image:: inc/logo-rail2x.svg
  :class: logo-rail2x

.. image:: inc/logo-mfund.png
  :class: logo-mfund

.. rst-class:: build fade

* *Vehicle-to-Everything*\ -Kommunikation (V2X) im Bahnwesen

  * analog: Car2X, Ship2X, Airplane2X, …, Rail2X
  * Sicherheit, Effizienz, Komfort durch Vernetzung von Fahrzeugen
    und Infrastruktur

* V2X-Funk nutzt spezielle Standards und Protokolle
  :understate:`(z. B. WLAN IEEE 802.11p)`
* drei Anwendungsfälle

  * Bedarfshalt :understate:`(Funk statt Kabel zum Signalisieren an Tf
    + Rückmeldung an Fahrgäste)`
  * Anrufschranke :understate:`(automatisches Anmelden und Abmelden von
    Kfz etc. per Funk)`
  * prädiktive Wartung :understate:`(Fz sammeln Daten auf der Strecke
    ein, WAN ohne Internet)`

* .. rst-class:: logo-banner

    * .. image:: inc/logo-db.svg
    * .. image:: inc/logo-hpi-no-txt.svg
    * .. image:: inc/logo-siemens.svg
    * .. image:: inc/logo-dlr.svg
    * .. image:: inc/logo-dralle.png



.. nextslide::

.. rst-class:: build

  * Feld-Tests und Abschluss-Demonstration im Digitalen Testfeld
    :understate:`(Bf. Scheibenberg et al.)`
  * Bearbeitung der Themen in der ersten Digital Rail Summer School
    :understate:`(2019)`

.. raw:: html

  <iframe
    class="rail2x not-in-print"
    src="https://player.vimeo.com/video/413134359?h=a54b87b17a&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479#t=1m50s"
    frameborder="0"
    allow="autoplay; fullscreen"
    allowfullscreen>
  </iframe>

.. rst-class:: center only-in-print

  |

  eingebettetes Video, siehe:

  https://rail2x.berlin/

  |



.. rst-class:: right-title

Komfort am BÜ
=============

.. image:: inc/bü-beschrankt.jpg
  :class: bottom left background
  :width: 100%



.. rst-class:: right-title

Aufmerksamkeit am BÜ
====================

.. image:: inc/bü-unbeschrankt.jpg
  :class: bottom left background
  :width: 100%



.. rst-class:: no-title no-footer no-slide-no

DiAK: Das digitale Andreaskreuz
===============================

.. image:: inc/diak-wort-und-bild-auf-schwarz.svg
  :align: center
  :width: 40%
  :class: not-in-print

.. image:: inc/diak-wort-und-bild-auf-weiß.svg
  :align: center
  :width: 40%
  :class: only-in-print

.. rst-class:: smaller bottom left

  * Jossekin Beilharz, Paul Geppert, *Lukas Pirl*, and Andreas Polze
  * Digital Rail Summer School 2022
  * Professur für Betriebssysteme und Middleware
  * Hasso-Plattner-Institut, Universität Potsdam



DiAK: Das digitale Andreaskreuz
===============================

.. image:: inc/diak-bild-schwarz.svg
  :width: 20%
  :class: right not-in-print

.. image:: inc/diak-bild-weiß.svg
  :width: 20%
  :class: right only-in-print

* Erhöhung der Aufmerksamkeit, Effizienz und des Komforts an BÜs

.. rst-class:: build fade

* Digitalisierung der BÜs und Fz

  * zur Vernetzung mit Straßenverkehr

* .. rst-class:: logo-banner

    * .. image:: inc/logo-db.svg
    * .. image:: inc/logo-hpi-no-txt.svg
    * .. image:: inc/logo-siemens.svg
    * .. image:: inc/logo-dlr.svg
    * .. image:: inc/logo-bmw-group.svg

* Interoperabilität zwischen Technologien der Verkehrstelematik

  * V2X: basierend auf IEEE 802.11p und IEEE 1609
    :understate:`(ITS-G5)`
  * C-V2X: basierend auf Mobilfunk & Internet
    :understate:`(4G/LTE, 5G, Edge/Cloud)`

* Förderung vom Bundesministerium für Verkehr und digitale Infrastruktur
  (BMVI)
* Teil vom Reallab Hamburg (RealLabHH)



RealLabHH: Reallabor Hamburg
============================

.. image:: inc/logo-reallabhh-schwarz.svg
  :width: 20%
  :class: right not-in-print

.. image:: inc/logo-reallabhh-weiß.svg
  :width: 20%
  :class: right only-in-print

* DiAK ⊂ RealLabHH
* Erprobung digitaler Mobilität

.. rst-class:: build

* Erarbeitung von Handlungsempfehlungen

  * für umwelt- und klimagerechte Umgestaltung von Mobilität

* 10 Projekte\ :understate:`, 31 Partner`

  * Plattformen zur Vernetzung, vernetzte Dienstleistungen,
    autonomes Fahren, …

* Vorstellung der Ergebnisse auf dem ITS Weltkongress

  .. image:: inc/ITS-Hamburg-21.svg
    :width: 25%
    :class: bottom right

  * Oktober 2021, Hamburg

* Teil der Nationalen Plattform der Mobilität (NPM)



NPM: Nationale Plattform der Mobilität
======================================

.. image:: inc/logo-npm-schwarz.svg
  :width: 20%
  :class: right not-in-print

.. image:: inc/logo-npm-weiß.svg
  :width: 20%
  :class: right only-in-print

* DiAK ⊂ RealLabHH ⊂ NPM

* Ort der Diskussion

  * für bezahlbare, nachhaltige und klimafreundliche Mobilität

.. rst-class:: build

* 6 Arbeitsgruppen

  .. rst-class:: understate

    1. Klimaschutz im Verkehr
    2. alternative Antriebe und Kraftstoffe für Nachhaltigkeit

  3. :highlight2:`Digitalisierung für den Mobilitätssektor`

  .. rst-class:: understate

    4. Sicherung des Mobilitäts- und Produktionsstandortes
    5. Verknüpfung der Verkehrs- und Energienetze
    6. Standardisierung, Normung, Zertifizierung und Typgenehmigung



NPM AG 3: Digitalisierung Mobilitätssektor
==========================================

* Steigerung ökologische :positive:`Nachhaltigkeit`

  * Reduktion von Emissionen und Immissionen

* Erfüllung individueller :positive:`Mobilitätsbedürfnisse`

  * Schaffung von einfachen, schnellen und bezahlbaren Mobilitätsangeboten

* Steigerung :positive:`Effizienz`

  * nahtlose, komfortable und übergreifende Verkehrsströme

* Erarbeitung :positive:`Technologie`\ -Voraussetzungen

  * für Infrastruktur, Vernetzung und Befähigung von Verkehrsträgern

* Steigerung :positive:`Verkehrssicherheit`



zurück zu DiAK
==============

.. list-table::
  :widths: 100 25 150 25 250 50

  - - .. image:: inc/logo-npm-schwarz.svg
        :width: 100%
        :class: not-in-print

      .. image:: inc/logo-npm-weiß.svg
        :width: 100%
        :class: only-in-print

    - .. spacer

    - .. image:: inc/logo-reallabhh-schwarz.svg
        :width: 100%
        :class: not-in-print

      .. image:: inc/logo-reallabhh-weiß.svg
        :width: 100%
        :class: only-in-print

    - .. spacer

    - .. image:: inc/diak-wort-und-bild-auf-schwarz.svg
        :width: 100%
        :class: not-in-print

      .. image:: inc/diak-wort-und-bild-auf-weiß.svg
        :width: 100%
        :class: only-in-print

    - .. spacer



.. include:: _diak-in-hh.rst



DiAK Feldtests
==============

.. raw:: html

  <iframe
    class="not-in-print"
    src="https://player.vimeo.com/video/638978176?h=a54b87b17a&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479#t=5m"
    frameborder="0"
    allow="autoplay; fullscreen"
    allowfullscreen>
  </iframe>

.. rst-class:: center only-in-print

  |

  eingebettetes Video, siehe:

  https://osm.hpi.de/diak/

  |



Aspekte im Projekt
==================

.. rst-class:: build fade

- Daten-Abgriff und Ausrüstung am technisch gesicherten BÜ
- ITS-G5 / V2X

  - Erprobung einer Onboard-Unit für Kfz
  - Erprobung einer Onboard-Unit für Tfz
  - Analyse und Definition der V2X-Nachrichtentypen
  - Simulation der Abdeckung von Roadside-Units

- C-V2X

  - Erprobung einer Implementierung & Anzeige
  - Erprobung Cloud-Infrastruktur & Routing (z. B. *geo fencing*)

- Übersetzung von ITS-G5 nach C-V2X



Systemarchitektur
=================

.. image:: inc/diak-systemarchitektur.svg
  :width: 100%
  :class: invert



ITS-G5 & C-V2X
==============

* Verfügbarkeit

  * ITS-G5: VW-Gruppe, Volvo, Scania, MAN, Siemens, …
  * C-V2X: BMW, Mercedes

.. rst-class:: build

* Alternativen / verwandte Arbeiten

  * *Utilizing Multi-Access Edge Computing Nodes*

    .. rst-class:: smaller

      * L\. G. Baltar, M. Mueck, D. Sabella: "Heterogeneous Vehicular
        Communications-Multi-Standard Solutions to Enable
        Interoperability", Oct. 2018, 10.1109/CSCN.2018.8581726

  .. rst-class:: build

  * *dual interface vehicles*

    .. rst-class:: smaller

    * K\. Z. Ghafoor, M. Guizani, L. Kong, H. S. Maghdid, K. F. Jasim:
      "Enabling Efficient Coexistence of DSRC and C-V2X in Vehicular
      Networks", April 2020, DOI: 10.1109/MWC.001.1900219

  * angepasster Standard

    .. rst-class:: smaller

    * Z. H. Mir, J. Toutouh, F. Filali, Y. Ko: "Enabling DSRC and C-V2X
      Integrated Hybrid Vehicular Networks: Architecture and Protocol",
      Oct. 2020, DOI: 10.1109/ACCESS.2020.3027074



.. include:: _diak-in-hh.rst



ITS Weltkongress
================

.. image:: inc/its-2.jpg
  :width: 100%
  :class: bottom background



.. nextslide::

.. image:: inc/its-1.jpg
  :width: 95%
  :class: bottom background




DiAK: Das digitale Andreaskreuz
===============================

.. image:: inc/diak-bild-schwarz.svg
  :width: 20%
  :class: right not-in-print

.. image:: inc/diak-bild-weiß.svg
  :width: 20%
  :class: right only-in-print

* Erhöhung der Aufmerksamkeit, Effizienz und des Komforts an BÜs

* Digitalisierung der BÜs und Fz

  * zur Vernetzung mit Straßenverkehr

* .. rst-class:: logo-banner

    * .. image:: inc/logo-db.svg
    * .. image:: inc/logo-hpi-no-txt.svg
    * .. image:: inc/logo-siemens.svg
    * .. image:: inc/logo-dlr.svg
    * .. image:: inc/logo-bmw-group.svg

* Interoperabilität zwischen Technologien der Verkehrstelematik

  * V2X: basierend auf IEEE 802.11p und IEEE 1609
    :understate:`(ITS-G5)`
  * C-V2X: basierend auf Mobilfunk & Internet
    :understate:`(4G/LTE, 5G, Edge/Cloud)`

.. rst-class:: build

* hybrides, co-simuliertes Testen
  :understate:`(inkl. sog. "software-defined radios")`
* Bereitstellen, Pflegen und kontinuierliches Testen von
  Linux-Treiber-Patches



\
=



.. rst-class:: middle-title

<backup>
========



Anwendungen für kooperative Verkehrstelematik
=============================================

.. rst-class:: understate

  *Intelligent Transportation Systems* (ITS)

* Kollisionsvermeidung
* Warnung vor Einsatzfahrzeugen
* Hinweise zu Ampelphasen
* Vorrangschaltung für Einsatzfahrzeuge an Ampeln
* Platooning
* autonomes Fahren
* …
* DiAK



hybride Co-Simulationen
=======================

.. image:: inc/hybrid-co-simulation.svg
  :align: center
  :width: 85%



Testbed für das Internet der Dinge
==================================

.. image:: inc/marvis-figure1.svg
  :class: invert bottom right
  :width: 85%



.. nextslide::

.. literalinclude:: inc/marvis-diak.py
  :language: python



.. nextslide::

.. image:: inc/USRP-B210.png
  :align: center
  :width: 75%



IEEE 802.11p
============

* V2X-Standard in Europa und den USA

  * ~ 5,9 GHz, 7 Kanäle, 10 MHz Bandbreite
  * keine Assoziation zu einem Netzwerk
  * keine Authentifizierung

  * "OCB-Modus"
    (Outside the Context of a BSS, :understate:`d. h.: Rundfunk`)

    * d\. h. anders als Access-Punkt-Modus
      :understate:`(basic service set (BSS) mode)`
    * d\. h. anders als Ad-hoc-Modus
      :understate:`(independent basic service set (IBSS) mode)`


* wenige Chips können 5,9 GHz, Linux-Treiber müssen angepasst werden

  * .. graphviz:: inc/802.11p-stack-linux.dot
      :class: top right ieee11p-stack
  * Treiber-Patches öffentlich bereitgestellt
  * kontinuierliches Testen der Patches im IoT-Labor des HPIs

    .. image:: inc/logo-iot-lab.svg
      :class: bottom right
      :width: 12%
