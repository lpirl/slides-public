.. include:: projector-and-presenter-test-slides.rst



.. rst-class:: no-footer no-slide-no middle-title

==================================
fault-injection-driven development
==================================

.. image:: inc/logo-hpi-no-txt.svg
  :class: top right
  :width: 7.5%

.. rst-class:: bottom left small

* Lukas Pirl
* 23.09.2020
* Digital Rail Summer School 2020
* Professorship for Operating Systems and Middleware
  of Prof. Dr. Andreas Polze
* Hasso Plattner Institute at the University of Potsdam, Germany



fault-injection-driven development
==================================

.. rst-class:: build

* make dependability a first-class concern during development

  .. rst-class:: build fade

  * i.e., integrated in development processes,
    have dependability and fault models,
    …
  * provoke failure

* .. image:: inc/fidd-one-environment.svg
    :width: 95%
    :align: center

  .. rst-class:: ref right

    Dr. Lena Feinbube



.. nextslide::
.. rst-class:: no-footer

* .. image:: inc/fidd-one-environment-highlight-suc.svg
    :width: 95%
    :align: center



status quo
==========

.. rst-class:: build fade

* fault-tolerant systems *do* fail

  .. rst-class:: build fade

  * 8h Azure outage 2012

    * leap day bug in SSL certificate generation

  * 4.5h Amazon S3 outage 2017

    * typo in manual command took "too many" servers down

  * .. image:: https://www.rbb24.de/content/dam/rbb/rbb/rbb24/2020/2020_01/rbb-reporter/Stoerung.jpg.jpg
      :width: 33%
      :class: top right

  * ...

* systems become more …

  * … complex
  * … critical
  * .. image:: https://image.jimcdn.com/app/cms/image/transf/dimension=origxorig:format=jpg/path/s818278d0e3dbbbe6/image/i8a75ba32674bd3a8/version/1520506024/image.jpg
      :width: 33%
      :class: bottom right build

* => ensuring dependability crucial and challenging

.. rst-class:: smallest understate bottom left

  * image of upper image: rbb/Donschen
  * image of lower image: https://www.info24news.net/dtannaberg/






=============
dependability
=============

.. rst-class:: large build

* justifiably trust the system to fulfill its specification



dependability
=============

a taxonomy

.. graphviz:: inc/dependability.dot
  :class: bottom right

.. rst-class:: smaller bottom left gray

  * A\. Avižienis, J\.-C\. Laprie, and B\. Randell,
  * "Dependability and Its Threats: A Taxonomy,"
  * in Building the Information Society,
  * Springer, Boston, MA, 2004, pp. 91–120.



dependability attributes
========================

.. image:: inc/fidd-one-environment-highlight-dependability-measurements.svg
  :width: 95%
  :align: center



.. rst-class:: no-title
.. nextslide::

.. image:: inc/fidd-one-environment-highlight-dependability-measurements.svg
  :width: 25%
  :class: top right

.. graphviz:: inc/dependability-highlight-attributes.dot
  :class: bottom left



dependability threats
=====================

.. image:: inc/fidd-one-environment-highlight-fault-models.svg
  :width: 95%
  :align: center



.. rst-class:: no-title
.. nextslide::
.. image:: inc/fidd-one-environment-highlight-fault-models.svg
  :width: 25%
  :class: top right

.. graphviz:: inc/dependability-highlight-threats.dot
  :class: bottom left



.. nextslide::
.. image:: inc/fidd-one-environment-highlight-fault-models.svg
  :width: 25%
  :class: top right

.. rst-class:: build fade

* fault :gray:`(Fehlerursache)`

  * adjudged or hypothesized error cause

    * in software: bugs/defects

  * might *activate* an error

* error  :gray:`(Fehlerzustand)`

  * incorrect system state
  * might *propagate* to a failure

* failure  :gray:`(Ausfall)`

  * deviation from specification
  * might appear as fault to related systems



.. nextslide::
.. image:: inc/fidd-one-environment-highlight-fault-models.svg
  :width: 25%
  :class: top right

.. rst-class:: build fade

* single component view

  .. graphviz:: inc/fault-error-failure.dot
    :align: center

* systems of systems view

  .. graphviz:: inc/fault-error-failure-sos.dot
    :align: center



fault model
===========

.. image:: inc/fidd-one-environment-highlight-fault-models.svg
  :width: 25%
  :class: top right

* set of faults assumed to occur

  .. rst-class:: build

  * hardware faults

    * well-established fault models

      * bit flips:
        :gray:`single xor multi`

      * stuck-at faults:
        :gray:`a bit permanently set to 1 (stuck-at-1) xor 0 (stuck-at-0)`

      * bridging faults:
        :gray:`two signals are connected although they shouldn't be`

      * delay faults:
        :gray:`delay of a path exceeds clock frequency`

  * software faults

    * less well-established fault models

      .. rst-class:: build fade

      * crash
      * computing
      * timing / omission


dependability models
====================

.. image:: inc/fidd-one-environment-highlight-dependability-models.svg
  :width: 95%
  :align: center



.. rst-class:: no-title
.. nextslide::
.. image:: inc/fidd-one-environment-highlight-dependability-models.svg
  :width: 25%
  :class: top right

.. graphviz:: inc/dependability-highlight-means.dot
  :class: bottom left



.. nextslide::

.. rst-class:: build fade

* expresses dependability dependencies between components

  * esp. fault tolerance mechanisms

* model types: reliability block diagrams, fault trees, …

* .. image:: inc/fault-tree-example.svg
    :class: invert bottom right
    :width: 40%



fault injection
===============

.. image:: inc/fidd-one-environment-highlight-experiments.svg
  :width: 95%
  :align: center



fault injection
===============

.. image:: inc/fidd-one-environment-highlight-experiments.svg
  :width: 25%
  :class: top right

.. rst-class:: build fade

* experimental dependability assessment

  * promises lower complexity

    * compared, e.g., to formal verification

* fault injection ⊂ testing
* try to prove the system :negative:`wrong`

  * instead of trying to prove the system :positive:`correct`
    :gray:`as with "traditional" testing`

  * counter developers' bias towards happy cases

* concept

  #. forcefully activate faults :gray:`(i.e., "inject")`

     .. image:: inc/pull-plug.png
       :width: 20%
       :class: bottom right

  #. assess delivered quality of service



Rail2X
======

.. image:: inc/logo-rail2x.svg
  :class: logo-rail2x

.. image:: inc/logo-mfund.png
  :class: logo-mfund

.. rst-class:: build fade

* Vehicle-to-X communication (V2X) for railways (Rail2X)

  * smart mobility through interconnection of vehicles and infrastructure
  * analogously: Car2X, Ship2X, Airplane2X

* V2X :gray:`(hence, Rail2X)` uses specialized WiFi standard IEEE 802.11p

  * pre-defined messages :gray:`(e.g., emergency vehicle alert)`
  * focus on low latency :gray:`(compared to, e.g., 802.11n)`
  * no access points :gray:`(similar to ad-hoc mode)`
  * higher range :gray:`(up to 1 km)`


.. rst-class:: logo-banner

  * .. image:: inc/logo-db.svg
  * .. image:: inc/logo-siemens.svg
  * .. image:: inc/logo-dlr.svg
  * .. image:: inc/logo-hpi-no-txt.svg
  * .. image:: inc/logo-dralle.png



fault injection: example of Rail2X
==================================

.. image:: inc/fidd-one-environment-highlight-experiments.svg
  :width: 25%
  :class: top right

.. rst-class:: build fade

* .. graphviz:: inc/rail2x-use-case-1.dot
    :class: left

* .. graphviz:: inc/rail2x-use-case-1-jamming.dot
    :class: right

* .. graphviz:: inc/802.11p-jamming.dot
    :class: bottom right

* .. image:: inc/el20-no-bkg.png
    :class: bottom left
    :width: 25%



.. rst-class:: no-title no-slide-no

demo
----

|

.. rst-class:: larger bold

  demo

.. image:: inc/iot-lab-logo-2018.svg
  :class: top right
  :width: 10%

.. graphviz:: inc/802.11p-jamming.dot
    :align: center



IEEE 802.11p packet round trip times while jamming
--------------------------------------------------

.. image:: inc/iot-lab-logo-2018.svg
  :class: top right
  :width: 7.5%

.. rst-class:: build fade

* .. image:: inc/richter18performance-figure5.svg
    :class: invert
    :width: 100%

* packet round trip times are lowest using IEEE 802.11p OCB mode

  * esp. with lowest standard deviation

    * :understate:`desirable for soft real-time applications`

.. rst-class:: ref left

  Richter D, Pirl L, Beilharz J, Werling C, Polze A. Performance of
  Real-TimeWireless Communication for Railway Environments with IEEE
  802.11 p.


fault-injection-driven development
==================================

.. image:: inc/fidd-one-environment.svg
  :width: 90%
  :align: center

.. rst-class:: ref right

  Lena Feinbube

.. nextslide::
.. rst-class:: build fade

* in three stages

  * simulation for scaling, test bed for controllability,
    field tests for representativeness

    |

* .. image:: inc/fidd-three-environments.svg
    :width: 85%
    :align: center



success stories
===============

.. rst-class:: build

* Linux kernel

  * e.g., through syscall fuzzing

* ISO 26262 :gray:`(Road vehicles – Functional safety)`
  recommends fault injection
* software fault injection in production

  .. rst-class:: build

  * Netflix

    * Chaos Monkey

      * terminates AWS EC2 instances
      * during business hours only

        * staff is watching and can react quickly

  * Etsy :gray:`(e-commerce)`

* *chaos engineering* offered as a service by major Cloud providers



.. rst-class:: no-footer

fault-injection-driven development
==================================

.. image:: inc/logo-hpi-no-txt.svg
  :class: top right
  :width: 7.5%

.. image:: inc/fidd-three-environments.svg
  :width: 95%
  :align: center

.. rst-class:: bottom left smaller gray

* Lukas Pirl
* 23.09.2020
* Digital Rail Summer School 2020
* Professorship for Operating Systems and Middleware
  of Prof. Dr. Andreas Polze
* Hasso Plattner Institute at the University of Potsdam, Germany
