.. include:: projector-and-presenter-test-slides.rst

=======================
Rail2X – Smart Services
=======================

.. rst-class:: smaller

* Workshop Datenbankarchitekturen mFUND-Begleitforschung
* 20.06.2018
* |
* Lukas Pirl, Jossekin Beilharz, Lena Feinbube, Sven Köhler, and
* Prof. Dr. rer. nat. habil. Andreas Polze
* Operating Systems & Middleware Group
* Hasso Plattner Institute at the University of Potsdam, Germany

.. hack because of https://github.com/lpirl/hieroglyph-themes/issues/9
.. raw:: html

  <style>
  .white-background-se {
    background-image: linear-gradient(149deg, rgba(0,0,0,0) 50%, white 50%);
  }
  img.hpi-logo {
    padding: 6em 2em 4em 14.2em;
  }
  #rail2x-smart-services .slide-footer {
    display: none;
  }
  h1 {
    text-align: center;
  }
  </style>

.. image:: inc/logo-hpi.svg
  :class: bottom right white-background-se hpi-logo

.. rst-class:: logo-banner right bottom

  * .. image:: inc/logo-db.svg
  * .. image:: inc/logo-siemens.svg
  * .. image:: inc/logo-dlr.svg
  * .. image:: inc/logo-dralle.png

Hasso Plattner Institute
========================

.. rst-class:: build fade

* Digital Engineering faculty of the University of Potsdam
* established 1998 by Hasso Plattner, co-founder of SAP and the institute's donor
* .. image:: https://hpi.de/fileadmin/user_upload/hpi/bilder/personen/professoren/hasso_plattner_gro%C3%9F_4992x3543.jpg
    :class: left bottom
    :width: 39%
* 500 students (BSc, MSc, PhD, Design Thinking), 12 chairs
* .. image:: https://hpi.de/fileadmin/user_upload/hpi/navigation/25_presse/25_bildmaterial/05_Fotos/10_campus/20110718_HPI_Luftbild_2244x1494.jpg
    :class: right bottom
    :width: 42%

Operating Systems & Middleware Group
====================================

* research topics include :gray:`(but not limited to)`

  .. rst-class:: build

  * middleware for predictable systems

    .. rst-class:: build fade

    * paradigms, design and implementation patterns
    * development practices
      :gray:`(e.g., continuous dependability assessments)`

    * .. image:: inc/fault-tree-example.svg
        :width: 35%
        :class: bottom right invert

    .. * time behaviour, security, fault tolerance, …

  * operating systems

    .. rst-class:: build fade

    * novel concepts for novel hardware paradigms

      .. * NUMA, IBM Power, Intel SCC

      * e.g., data partition strategies on NUMA systems

    * programming models for hybrid parallelism

      .. * CPU vs. Xeon Phi vs. Nvidia Tesla vs. Cluster

  * distributed systems

    .. rst-class:: build fade

    .. * service-oriented computing

    * heterogeneous, distributed systems

      * e.g., `CloudCL <https://github.com/osmhpi/cloudcl>`__:
        framework for parallel computing

  .. ~ * embedded systems

  .. ~   * analytical redundancy and online replacement
  .. ~   * dynamic (re-)configuration of components

Rail2X
======

.. hack because of https://github.com/lpirl/hieroglyph-themes/issues/9
.. raw:: html

  <style>
  img.mfund-logo {
    padding: 6em 2em 2.5em 10.1em;
  }
  </style>

.. image:: inc/logo-rail2x.svg
  :width: 17%
  :class: bottom right white-background-se mfund-logo

.. image:: inc/logo-mfund.png
  :width: 10%
  :class: bottom right

.. rst-class:: build

* joint project

  .. rst-class:: build fade

  * DB Systel, Siemens, German Aerospace Center (DLR), DRALLE Systementwicklungen

* implement Vehicle-to-X communication (V2X) for railways (Rail2X)

  .. rst-class:: build fade

  * V2X: smart mobility through interconnection of vehicles and infrastructure
  * analogously: Car2X, Ship2X, Airplane2X

* V2X :gray:`(hence, Rail2X)` uses specialized WiFi standard IEEE 802.11p

  .. rst-class:: build fade

  * pre-defined messages :gray:`(e.g., emergency vehicle alert)`
  * focus on low latency :gray:`(compared to, e.g., 802.11n)`
  * no access points :gray:`(similar to ad-hoc mode)`
  * higher range :gray:`(up to 1 km)`

Rail2X use cases
================

.. rst-class:: build

#. flag stops

   .. rst-class:: build fade

   * :negative:`current situation:`
     train must pass flag stops slowly, wastes time and energy

     .. * in order to be able to stop, if a passenger wants to board the train

   * :positive:`envisioned situation:`
     Rail2X-enabled button on platform to request train to stop

     .. * i.e., using a Rail2X-enabled button at the station platform

#. on-call crossings :gray:`(i.e., German "Anrufschranke")`

   .. rst-class:: build fade

   * :negative:`current situation:`
     to cross, car drivers must stop and get out of the car twice

   * :positive:`envisioned situation:`
     automatic operation through V2X between car and crossing

#. predictive maintenance

   .. rst-class:: build fade

   * :negative:`current situation:`
     maintenance independent from wear, wastes time

     .. rst-class:: gray

     * e.g., switches,
       for which monitoring with wired or mobile network would be uneconomic

   * :positive:`envisioned situation:`
     passing trains collect maintenance data using Rail2X

research topics in Rail2X
=========================

.. rst-class:: build

* middleware architectures

   .. rst-class:: build fade

  * abstract: e.g., store-and-forward, traceability
  * Rail2X: e.g., *"How to deal with flaky connections and unreliable storage?"*

* Internet of Things (IoT)

   .. rst-class:: build fade

  * abstract: e.g., trust :gray:`(centralized/certificates vs. decentralized/blockchain)`
  * Rail2X: e.g., *"How to ensure, that no malicious party can inject fake data?"*

* dependability

   .. rst-class:: build fade

  * abstract: e.g., fault models, dependability models, fault injection testing
  * Rail2X: e.g., *"How to justify, that the system can be trusted?"*

data in Rail2X
==============

.. rst-class:: build

* DLR plans to integrate other open data sources

    .. rst-class:: build fade

  * e.g., plausibility checks for weather sensors with open weather data
    (by DWD)
  * e.g., recognize/correlate track network in OpenStreetMap

* HPI focuses on the predictive maintenance use case

    .. rst-class:: build

  * maintenance data

    .. * no objections regarding sharing
    .. rst-class:: build

    * e.g., from sensors of infrastructure elements
    * input/output via APIs planned

      .. rst-class:: build

      * specifically, to exchange data with the DLR in the context of Rail2X

        .. rst-class:: build

        * DLR does research on a server to consolidate data of the
          Deutsche Bahn (different project)

      * probably employing techniques common in Web development

        .. rst-class:: build

        * because DLR's server is a Web server
        * e.g., RESTful, JSON/YAML/XML, etc.

.. nextslide::
  :increment:

* \

  * research data

    .. rst-class:: build

    * e.g., from experiments in the IoT lab at the HPI
    * raw data recorded rather sporadic than continuous
    * results shared through academic publications
    * tend to be very specific

      * i.e., hard to interpret without a lot of additional information

    * .. image:: inc/802.11-jamming-rtt.svg
        :width: 85%
        :class: invert
        :align: center

.. slide::
  :class: no-title

  |

  .. rst-class:: logo-banner

  * .. image:: inc/logo-rail2x.svg
  * .. image:: inc/logo-mfund.png

  |

  .. rst-class:: smaller gray

  * Workshop Datenbankarchitekturen mFUND-Begleitforschung
  * 20.06.2018
  * |
  * Lukas Pirl, Jossekin Beilharz, Lena Feinbube, Sven Köhler, and
  * Prof. Dr. Andreas Polze
  * Operating Systems & Middleware Group
  * Hasso Plattner Institute at the University of Potsdam, Germany

  |

  .. rst-class:: logo-banner

  * .. image:: inc/logo-hpi-no-txt.svg
  * .. image:: inc/logo-db.svg
  * .. image:: inc/logo-siemens.svg
  * .. image:: inc/logo-dlr.svg
  * .. image:: inc/logo-dralle.png
