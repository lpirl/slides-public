#!/usr/bin/env python3
# -*- coding: utf-8 -*-

extensions = [
    'hieroglyph',
#    'sphinx.ext.graphviz',
#    'sphinx.ext.imgmath',
#    'matplotlib.sphinxext.plot_directive',
]

# required, so that serving the slides under "/" works in the browser
master_doc = 'index'

# what to exclude when looking for source files
exclude_patterns = [
    '.DS_Store',
    'Thumbs.db',
    'README*',
    '**/README*',
    'build',
    'inc',
]

# required, so that the HTML title does not get suffixed with
# "… documentation"
html_title = ""

html_scaled_image_link = False

# those files are copied to the output directory, no matter if in use
# ~ html_static_path = ['static']

rst_prolog = '.. include:: prolog.rst'
rst_epilog = '.. include:: ../0-generic/epilog.rst'

slide_theme_path = ['themes']
slide_theme = 'simple-slides-dark'
slide_footer = '　'.join((
    'Rail2X – Smart Services',
    'Workshop Datenbankarchitekturen mFUND-Begleitforschung',
    'lukas.pirl@hpi.de',
    '20.06.2018',
))
slide_numbers = True

# you can place the custom CSS file in _static
# slide_theme_options = {'custom_css': 'presentation.css'}

pygments_style = 'monokai'

graphviz_output_format = 'svg'
graphviz_dot_args = [
    '-Gfontname=Ubuntu,sans',
    '-Efontname=Ubuntu,sans',
    '-Nfontname=Ubuntu,sans',
]

try:
    from matplotlib.sphinxext.plot_directive import TEMPLATE as PLOT_TEMPLATE
except ImportError:
    pass
else:
    plot_formats = ['svg']
    plot_template = PLOT_TEMPLATE + """
    .. only:: slides

      {% for img in images %}
      .. figure:: {{ build_dir }}/{{ img.basename }}.{{ default_fmt }}
        {% for option in options -%}
        {{ option }}
        {% endfor %}
        {{ caption }}
      {% endfor %}
    """

imgmath_image_format = 'svg'
