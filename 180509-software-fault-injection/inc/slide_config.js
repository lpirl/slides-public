var SLIDE_CONFIG = {
  // Slide settings
  settings: {
    useBuilds: true, // Default: true. False will turn off slide animation builds.
    enableSlideAreas: false, // Default: true. False turns off the click areas on either slide of the slides.
  },
};
