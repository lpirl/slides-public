#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date

extensions = ['sphinx.ext.graphviz', 'hieroglyph']

master_doc = 'index'
# what to exclude when looking for source files
exclude_patterns = [
    '.DS_Store',
    'Thumbs.db',
    'README*',
    '**/README*',
    'build',
    'inc',
]

html_title = ""
html_scaled_image_link = False

slide_theme_path = ['themes']
slide_theme = 'simple-slides-dark'
slide_footer = '　'.join((
    'Software Fault Injection',
    'Software Reliability Engineering SoSe18',
    'lukas.pirl@hpi.de',
    '09.05.2018',
))
slide_numbers = True

pygments_style = 'monokai'

graphviz_output_format = 'svg'
graphviz_dot_args = [
    '-Gfontname=Ubuntu,sans',
    '-Efontname=Ubuntu,sans',
    '-Nfontname=Ubuntu,sans',
]

rst_prolog = '.. include:: prolog.rst'
rst_epilog = '.. include:: ../0-generic/epilog.rst'
