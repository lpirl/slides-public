.. todo:

  * SFI characterization

    * controllability
    * repeatability
    * intrusiveness
    * efficiency
    * coverage

.. include:: projector-and-presenter-test-slides.rst

========================
software fault injection
========================

.. rst-class:: smaller

* Lukas Pirl, Daniel Richter, and Andreas Polze
* Lecture on Software Reliability Engineering
* Operating Systems & Middleware Group
* Hasso Plattner Institute at the University of Potsdam, Germany

fault-tolerant systems *do* fail
================================

.. rst-class:: build fade

* 2.5h Facebook outage 2010

   * "friendly" DDoS due to wrong configuration value

* 8h Azure outage 2012

   * leap day bug in SSL certificate generation

* 4.5h Amazon S3 outage 2017

   * typo in manual command took “too many” servers down

threats
=======

.. rst-class:: stamp

↻ recap

.. rst-class:: build fade

* fault :gray:`(Fehlerursache)`

  * adjudged or hypothesized error cause

    * in software: bugs/defects

  * might *activate* an error

* error  :gray:`(Fehlerzustand)`

  * incorrect system state
  * might *propagate* to a failure

* failure  :gray:`(Ausfall)`

  * deviation from specification
  * might appear as fault to related systems

fault activation
================

* fault activation of software highly dependent on environment

  .. rst-class:: build fade

  * hardware dependability
  * feature interaction
  * third party components

    * e.g., libraries

  * related services

    * e.g., remote APIs

  * user interaction

    * e.g., data input

  * …

dependability evaluation
========================

* two classes of approaches

  .. rst-class:: build fade

  * formal verification

    * prove software :positive:`correct`
    * requires formal specification

      * for all inputs (incl. environment states)
        → **combinatorial explosion**

  * testing

    * prove software :negative:`wrong`
    * discover bugs during runtime
    * requires a fault model

formal verification
-------------------

.. rst-class:: build fade

* increasingly hard

  * increasing complexity

    * higher technology stacks,
      tool chain :gray:`(e.g., compilers)`,
      composition,
      …

  * resource constraints

    * requirements change due to agile development,
      time-to-market pressure,
      …

* attractive for model-driven development

  .. rst-class:: small

  * i.e., model is specification, transformation is formally verified

* usually makes strong assumptions

  .. rst-class:: small

  * e.g., assume correct hardware for verification of ``seL4`` microkernel

* formally verified code might still not meet intentions

  .. rst-class:: small

  * e.g., 802.11i/WPA2 vulnerabilities despite (partial) formal verification

testing
-------

.. rst-class:: build

* widely adopted
* best practice
* extensive

  .. rst-class:: small build fade

  * unit testing,
    integration testing,
    regression testing,
    …

* :negative:`but:` developers/testers might be biased

  .. rst-class:: build

  * tests are expected to succeed

    .. rst-class:: build

    * code is crafted to to satisfy tests (TDD)
    * xor
    * tests are crafted to test code (non-TDD)

* → usually "testing in success space"

fault model
===========

* set of faults assumed to occur

  .. rst-class:: build

  * hardware faults

    * relatively established fault model

      .. rst-class:: build

      * bit flips:
        :gray:`single xor multi`

      * stuck-at faults:
        :gray:`a bit permanently set to 1 (stuck-at-1) xor 0 (stuck-at-0)`

      * bridging faults:
        :gray:`two signals are connected although they shouldn't be`

      * delay faults:
        :gray:`delay of a path exceeds clock frequency`

  * software faults

    * no commonly established fault model

      .. rst-class:: build

      * timing / omission
      * computing
      * crash

fault injection
===============

* fault injection ⊂ testing ¹

.. rst-class:: build fade

* experimental dependability assessment

  * idea: lower complexity

    * compared, e.g., to formal verification

* concept

  #. forcefully activate :gray:`(i.e., "inject")` faults

     * or, forcefully introduce errors

  #. assess delivered quality of service

.. rst-class:: smaller gray bottom

¹ no widely-accepted definition to differentiate between the two

history
-------

* not definitive, but to give an idea:

  .. rst-class:: build

  * ~1969 hardware fault injection at IBM

    * simulated to evaluate integrity of logic units during design

      * faults: stuck transistors, open/shorted diodes

  * 1970+ A. Avižienis: early theory on faults

    * coined "fault tolerance", classification, modeling, …

      * wanted operating system support for fault-tolerant hardware

.. rst-class:: ref

  * M\. Ball and F. Hardie, "Effects and detection of intermittent failures in digital systems," in Proceedings of the November 18-20, 1969, fall joint computer conference , 1969, pp. 329–335.
  * A\. Avizienis and D. A. Rennels, "Fault-tolerance experiments with the JPL STAR computer.," 1972.

:underline:`software` fault injection
=====================================

* implemented in software *and* targeting software ¹

  .. rst-class:: build fade small

  * != hardware-implemented fault injection (HWIFI)

    * targeting hardware, e.g., exposition to increased radiation

  * != software-implemented fault injection (SWIFI)

    * targeting hardware, e.g., flipping of bits in memory

.. rst-class:: build

* requires

  .. rst-class:: build

  * faultload

    * :highlight1:`which` faults (from :highlight1:`fault model`)
      to inject :highlight2:`when` and :highlight2:`where`
      (depends on :highlight2:`operational profile`)

  * workload

    * for realistic fault activation and error propagation

.. rst-class:: gray smaller bottom

¹ no widely-accepted definition here;
this is what I think makes sense;
feel free to question and have your own view

typical objectives
==================

.. rst-class:: build

* find "dependability bottlenecks" / single points of failure
* assess quality of service in presence of faults

  * performance degradation

    * e.g., bandwidth, latency

  * dependability attributes

    .. rst-class:: build

    * availability, reliability, safety, security, integrity, maintainability

* assess specific fault tolerance mechanisms

  * e.g., efficiency, effectiveness

* determine coverage of error detection and recovery

typical "meta objectives"
-------------------------

.. rst-class:: build fade

* experiences & confidence regarding dependability

  * e.g., developers, testers, operators, architects, best-practices, documentation

* bug fixes for fault tolerance mechanisms
* well-tested and -understood fault tolerance mechanisms
* **measurements**

  * only objective measures allow comparisons between different systems

    * thus, allow to judge improvement/worsening between different versions

implementation
==============

depends heavily on system under consideration

.. contents:: aspects to consider
    :local:
    :depth: 1

injection trigger
-----------------

.. rst-class:: build

* time-based

  .. rst-class:: build

  * absolute xor relative

    * e.g., absolute time of day, relative to run time

  * one-time vs. periodic vs. sporadic

    * e.g., fixed rate, between a minimum and a maximum rate

* location-based

  * depends on system under consideration and level of abstraction

    .. rst-class:: build

    * e.g., on access of specific memory areas,  specific nodes

* execution-driven

  * based on control flow during runtime

execution state during injection
--------------------------------

.. rst-class:: build

* prior to execution

  * e.g., code mutation, environment state, infrastructure

* during runtime

  * at library load time
  * software traps
  * hardware traps

target artifact
---------------

.. rst-class:: build fade

* source code

  * e.g., change control flow, add sleeps

* intermediate code representation

  * e.g., change operators or constants in bytecode

* binary representation

  * e.g., bit flips

* state

  * e.g., memory/storage modifications, edge-case states of environment

* environmental behavior

  * e.g., clock drift, node crashes, misbehaving hardware, related APIs' behavior

.. slide:: characteristics of different methods
  :level: 3

  different approaches have different advantages and disadvantages, e.g.:

  ================ ============ =============== ============ ===============
  \                        Hardware                     Software
  ---------------- ---------------------------- ----------------------------
  \                with contact without contact with contact without contact
  ================ ============ =============== ============ ===============
  cost              high        high            low           low
  perturbation      none        none            low           high
  risk of damage    high        low             none          none
  time resolution   high        high            high          low
  injection points  chip pin    chip internal   memory        memory
  \                                             software      IO controller
  controllability   high        low             high          high
  trigger           yes         no              yes           yes
  repeatability     high        low             high          high
  ================ ============ =============== ============ ===============

  .. rst-class:: ref

  M.-C. Hsueh, T. K. Tsai, and R. K. Iyer, "Fault injection techniques and tools," vol. 30, no. 4, pp. 75–82, Apr. 1997.


.. slide:: example injection targets for applications
  :level: 3

  .. rst-class:: build

  * black-box

    .. graphviz:: inc/fi-blackbox.dot
      :align: center

    .. rst-class:: build

    * :positive:`less` intrusiveness,
      :positive:`less` interference with result,
      :positive:`less` coupling,
      …

  * white-box

    .. graphviz:: inc/fi-whitebox.dot
      :align: center

    .. rst-class:: build

    * possibly :positive:`more` insights,
      :positive:`higher` performance,
      :positive:`easier` to debug,
      …

.. slide:: example injection targets for operating systems
  :level: 3

  .. image:: inc/os-technology-stack.svg
    :width: 90%
    :align: center

.. slide:: example injection targets for operating systems
  :level: 3

  .. image:: inc/os-technology-stack-fi-between-layers.svg
    :width: 90%
    :align: center

.. slide:: example injection targets for operating systems
  :level: 3

  .. image:: inc/os-technology-stack-fi-all.svg
    :width: 90%
    :align: center

.. slide:: example injection targets for Cloud stacks
  :level: 3

  .. image:: inc/cloud-technology-stack.svg
    :width: 90%
    :align: center

.. slide:: example injection targets for Cloud stacks
  :level: 3

  .. image:: inc/cloud-technology-stack-fi-in-layers.svg
    :width: 90%
    :align: center

.. slide:: example injection targets for Cloud stacks
  :level: 3

  .. image:: inc/cloud-technology-stack-fi-between-layers.svg
    :width: 90%
    :align: center

.. slide:: wrap-up
  :level: 1

adoption
========

.. rst-class:: build

* long-established for hardware testing
* partly adopted for software testing

  .. rst-class:: small build fade

  * missing accessibility?

    * e.g., tools not public, no documentation

  * tools too specialized?

    * e.g., on certain programming languages or APIs

  * available information too scattered/heterogeneous?

    * e.g., research prototypes, products, open-source projects

  * available information too heterogeneous?

    * e.g., heterogeneous wording makes it hard to find things

  * missing automation?

    * e.g., in comparison unit testing

FIDD: fault-injection-driven development
----------------------------------------

* incorporate software fault injection in development practices

  * in analogy and in addition to test-driven development

.. image:: inc/fidd.svg
  :width: 90%
  :align: center

* case study on OpenStack :gray:`(IaaS framework)`

.. rst-class:: ref right

  Lena Feinbube

success stories
---------------

* software fault injection in production

  .. rst-class:: build

  * Etsy :gray:`(e-commerce)`
  * Netflix

    .. rst-class:: build

    * Chaos Monkey

      .. rst-class:: build fade

      * terminates AWS EC2 instances

        * in AWS Auto Scaling Groups

      * during business hours only

        * staff is watching and can react quickly

software fault injection in production
--------------------------------------

* :positive:`pro`

  * staging environments inherently different from the production environment

    * likely to have an influence on results

  * less uncertainty

    * since no difference between staging and production environments

  * failures happen when staff is prepared
  * proven concept

    * e.g., in fire departments

  * awareness / critical analysis of own production system

.. nextslide::

* :negative:`con`

  .. rst-class:: build

  * risk

    * loosing data
    * frustrated customers
    * reputation
    * economic damage

  * testing is in place anyway
  * missing awareness?
  * lack of expertise?
  * unpredictable legacy systems?
  * …

For which dependability mean is software fault injection good for?
==================================================================

.. rst-class:: build-item-1

* fault prevention

  .. rst-class:: small build-item-5

  * :negative:`not directly`, but maybe for superordinate systems
    when practiced during development

.. rst-class:: build-item-1

* fault tolerance

  .. rst-class:: small build-item-6

  * :negative:`not directly`, but maybe fault tolerance mechanisms
    are added due to identified lack thereof

.. rst-class:: build-item-1

* fault removal

  .. rst-class:: small build-item-7

  * :positive:`yes`, to find bugs in existing fault tolerance mechanisms

.. rst-class:: build-item-1

* fault forecasting

  .. rst-class:: small build-item-8

  * :negative:`not directly`, but maybe through gain of awareness
    when creating the fault model

conclusion
==========

.. rst-class:: build

* specifically for the system under consideration:

  .. rst-class:: build fade

  * **What** to inject? → fault model

    .. rst-class:: small

    * bug trackers, vulnerability databases and failure reports can give inspiration

  * **When** to inject? → trigger

    .. rst-class:: small

    * likely to be chosen according to workload, when injecting during runtime

  * **Where** to inject? → dependability model

    .. rst-class:: small

    * know which faults *should* be tolerated, since
      there is usually not much gain from injecting non-tolerated faults

* have a clear **scope**

  .. rst-class:: smaller

  * considering all faults in all locations at all times in all layers
    of the technology stack is unrealistic

example: why you need a clear scope
===================================

.. rst-class:: build-fade

* assessing …

  * … data integrity :gray:`of a` …
  * … distributed storage application :gray:`on a` …

    * Tahoe-LAFS

  * … virtualized …
  * … high-availability …

    * using Ceph, among other things …

  * … :gray:`IaaS` Cloud platform …

    * OpenStack

.. rst-class:: bottom right

* .. graphviz:: inc/storage-stack-TahoeLAFS-on-virtualized-HA-OpenStack.dot
