#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import date

extensions = ['sphinx.ext.graphviz', 'hieroglyph']

master_doc = 'index'
# what to exclude when looking for source files
exclude_patterns = [
    '.DS_Store',
    'Thumbs.db',
    'README*',
    '**/README*',
    'glossary/*',
    'build',
    'inc',
]

rst_prolog = '.. include:: ../0-generic/prolog.rst'
rst_epilog = '.. include:: ../0-generic/epilog.rst'

html_title = ""
html_scaled_image_link = False

html_static_path = ['static']

slide_theme_path = ['themes']
slide_theme = 'simple-slides-dark'
slide_footer = '　'.join((
    'Software Fault Injection',
    'Embedded Operating Systems WiSe20',
    'lukas.pirl@hpi.de',
    '14.01.2021',
))
slide_numbers = True

# the files listed here in must be in ``html_static_path``
slide_theme_options = {'custom_css': 'custom.css'}

extensions.append('sphinx.ext.graphviz')
graphviz_output_format = 'png'
graphviz_dot_args = [
    '-Gfontname=Ubuntu,sans',
    '-Efontname=Ubuntu,sans',
    '-Nfontname=Ubuntu,sans',
]

try:
    from matplotlib.sphinxext.plot_directive import TEMPLATE as PLOT_TEMPLATE
except ImportError:
    pass
else:
    extensions.append('matplotlib.sphinxext.plot_directive')
    plot_formats = ['svg', 'png']
    plot_template = PLOT_TEMPLATE + """.. only:: slides

      {% for img in images %}
      .. image:: {{ build_dir }}/{{ img.basename }}.{{ default_fmt }}
        {% for option in options -%}
        {{ option }}
        {% endfor %}
        {{ caption }}
      {% endfor %}
    """
pygments_style = 'monokai'
